﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Health : MonoBehaviour
{
    [SerializeField]
    private float health;

    public int ScoreReward;

    public AudioClip deathClip;

    void Update()
    {
        if (health < 1)
            //GameplayManager.instance.AddScore(ScoreReward);
            Destroy(gameObject);
        //SoundManager.instance.PlaySoundFX(deathClip);
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Bullet")
        {
            health -= GameObject.Find("Player").GetComponent<PlayerController>().currentWeapon.damage;
            Destroy(target.gameObject);
        }
    }
}