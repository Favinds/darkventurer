﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Follow : MonoBehaviour {

    private List<Rigidbody2D> EnemyRBs;

    public float speed;

    private Transform PlayerPos;
    private Rigidbody2D rb;

    private float repelRange = .5f;


	// Use this for initialization
	void Awake () {

        PlayerPos = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();

        if(EnemyRBs == null)
        {
            EnemyRBs = new List<Rigidbody2D>();
        }

        EnemyRBs.Add(rb);
	}
	
    void OnDestroy()
    {
        EnemyRBs.Remove(rb);
    }

	// Update is called once per frame
	void Update () {

        if (Vector2.Distance(transform.position, PlayerPos.position) > 0.2f)
            transform.position = Vector2.MoveTowards(transform.position, PlayerPos.position, speed * Time.deltaTime);
	}

    void FixedUpdate()
    {
        Vector2 repelForce = Vector2.zero;
        foreach(Rigidbody2D enemy in EnemyRBs)
        {
            if (enemy == rb)
                continue;

            if(Vector2.Distance(enemy.position, rb.position) <= repelRange)
            {
                Vector2 repelDir = (rb.position - enemy.position).normalized;
                repelForce += repelDir;
            }
        }
    }
}
