﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private float speed = 5;
    public float timer;

    private Vector2 dir;

	// Use this for initialization
	void Start () {

        dir = GameObject.Find("Dir").transform.position;
        transform.position = GameObject.Find("FirePoint").transform.position;
        transform.eulerAngles = new Vector3(0, 0, GameObject.Find("Player").transform.rotation.eulerAngles.z);
	}
	
	// Update is called once per frame
	void Update () {

        timer += 1.0F * Time.deltaTime;
        if (timer >= 4)
        {
            GameObject.Destroy(gameObject);
        }

        transform.position = Vector2.MoveTowards(transform.position, dir, speed * Time.deltaTime);
	}
}
