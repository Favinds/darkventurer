﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;

    public GameObject Bullet;
    public Weapon currentWeapon;

    private Animator legAnim;
    private Animator anim;

    private Rigidbody2D rb;
    private Vector2 moveVelocity;

    private float nextTimeofFire = 0;

    [SerializeField]
    private int health;
    private bool hit = true;
 

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        legAnim = transform.GetChild(2).GetComponent<Animator>();
        anim = GetComponent<Animator>();
        transform.GetChild(3).GetComponent<SpriteRenderer>().sprite = currentWeapon.currentWeaponSpr;

    }

    private void Update()
    {
        Rotation();
        //Shoot
        if (Input.GetMouseButton(0))
        {
            if (Time.time >= nextTimeofFire)
            {
                currentWeapon.Shoot();
                nextTimeofFire = Time.time + 1 / currentWeapon.fireRate;
            }
        }

        //Bounds
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -19.5f, 19.5f), Mathf.Clamp(transform.position.y, -9.5f, 9.5f));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Movement();

    }

    void Rotation()
    {
        Vector2 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10 * Time.deltaTime);
    }

    void Movement()
    {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * speed;
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);

        if (moveVelocity == Vector2.zero)
            legAnim.SetBool("Moving", false);
        else
            legAnim.SetBool("Moving", true);
    }

    IEnumerator HitBoxOff()
    {
        hit = false;
        anim.SetTrigger("Hit");
        yield return new WaitForSeconds(1.5F);
        hit = true;
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Enemy")
        {
            if (hit) {
                StartCoroutine(HitBoxOff());
                health--;
                Destroy(GameObject.Find("Life-Box").transform.GetChild(0).gameObject);
                if (health < 1)
                {
                    StartCoroutine(Death());
                }
            }
            
        }
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 

    }
}

 